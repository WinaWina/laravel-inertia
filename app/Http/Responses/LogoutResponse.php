<?php 

namespace App\Http\Responses;

use Laravel\Fortify\Contracts\LogoutResponse as LogoutResponseContarct;

class LogoutResponse implements LogoutResponseContarct
{

    /**
     * toResponse
     * 
     * @param mixed $request
     * @return void
     */
    public function toResponse($request)
    {
        return redirect('/login');
    }
}