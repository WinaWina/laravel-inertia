<?php

namespace App\Models;

use Carbon\Carbon;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use illuminate\database\Eloquent\casts\Attribute;

class profits extends Model
{
    use HasFactory;
    /**
     * fillable
     * 
     * @var array
     */
    protected $fillable = [
        'transaction_id', 'total'
    ];

    /**
     * transavtions
     * 
     * @return void
     */
    public function transactions()
    {
        return $this->belongsTo(transactions::class);
    }
}